import React from 'react';
import {StyleSheet, View, Image, AsyncStorage, Text, BackHandler, ToastAndroid } from 'react-native';
import Main from './main/main';
import Berita from './main/berita';
import BeritaDetail from './main/beritadetail';
import Saran from './main/saran';
import Web from './main/web';
import { Router, Scene, Actions } from 'react-native-router-flux';

let backButtonPressedOnceToExit = false;

export default class route extends React.Component{
   
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
      }
    
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
    }
    
    onBackPress() {
        if (backButtonPressedOnceToExit) {
            BackHandler.exitApp();
        } else {
            if (Actions.currentScene !== 'main') {
                Actions.pop();
                return true;
            } else {
                backButtonPressedOnceToExit = true;
                ToastAndroid.show("Tekan sekali lagi untuk keluar aplikasi", ToastAndroid.SHORT);
                //setting timeout is optional
                setTimeout(() => { backButtonPressedOnceToExit = false }, 2000);
                return true;
            }
        }
    }

    render() {
        
        return (
          <Router backAndroidHandler={this.onBackPress}>
            <Scene key="root">
              <Scene key="main" component={Main}  initial={true} />
              <Scene key="berita"  component={Berita}/>
              <Scene key="beritadetail"  component={BeritaDetail}/>
              <Scene key="saran"  component={Saran}/>
              <Scene key="web"  component={Web}/>
            </Scene>
          </Router>
        );
      }
}