import React from 'react';
import { Platform, StyleSheet, Text, View, Image, Alert, AsyncStorage, ScrollView } from 'react-native';
import { Button, Form, Item, Input, Label, Spinner } from 'native-base';
import axios from 'axios';
import RNRestart from 'react-native-restart'; 

export default class Registrasi extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            nik : '',
            nama : '',
            no_hp : '',
            email : '',
            alamat : '',
            periksaData : true,
            isLoading: false,
        }
    }

    registrasi = () => {

        if(this.state.nik == '' || this.state.nik == '' || this.state.nik == '' || this.state.nik == '' || this.state.nik == ''){
            alert("Lenkapi Data Anda");
        }else{

            this.setState({ isLoading: true })

            const data = new FormData();
            const { nik, nama, email, no_hp, alamat} = this.state;
            data.append('nik', nik);
            data.append('nama', nama);
            data.append('email', email);
            data.append('no_hp', no_hp);
            data.append('alamat', alamat);

            axios.get('https://dinartech.com/simpus/api/create_android/'+nik+'/'+nama+'/'+email+'/'+no_hp+'/'+alamat)
            //axios.get('http://10.0.2.2:8000/api/create_android/'+nik+'/'+nama+'/'+email+'/'+no_hp+'/'+alamat)
                .then(res => {
                    this.setState({ isLoading: false })
                    AsyncStorage.setItem('nik', nik);
                    Alert.alert(
                        'Pendaftran Berhasil',
                        'Terima Kasih, Anda berhasil mendaftar di aplikasi Puskemas Tebas',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    RNRestart.Restart();
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                })

        }   

    }

    render(){
        const {periksaData, isLoading} = this.state;
        return(
            <View style={styles.container}>
                <ScrollView>
                 <Image
                    source={require('./../assets/logo.png')}
                    style={{ width: 125, height: 110,  marginBottom: 10, alignSelf:'center' }} />
                <Text style={{ fontWeight:'bold',  marginBottom: 60, alignSelf:'center' }}>PUSKESMAS TEBAS</Text>
                <Form>
                    <Item stackedLabel>
                        <Label>Nomor Induk Kependudukan (NIK)</Label>
                        <Input
                            onChangeText={nik => this.setState({ nik })}
                            maxLength={16}
                            keyboardType='numeric'
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Nama Lengkap</Label>
                        <Input
                            onChangeText={nama => this.setState({ nama })}
                            
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Email</Label>
                        <Input
                            onChangeText={email => this.setState({ email })}
                            
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Nomor Handphone</Label>
                        <Input
                            onChangeText={no_hp => this.setState({ no_hp })}
                            
                        />
                    </Item>
                    <Item stackedLabel>
                        <Label>Alamat</Label>
                        <Input
                            onChangeText={alamat => this.setState({ alamat })}
                        />
                    </Item>
                    {isLoading
                        ? <View><Spinner color='blue' />
                            {periksaData
                                ? <Text style={{ alignSelf: 'center' }}>Memeriksa Data Anda</Text>
                                : <Text style={{ alignSelf: 'center' }}>Menyiapkan Data Anda</Text>
                            }</View>
                        : <Button block iconLeft primary style={{ marginTop: 20 }} onPress={() => this.registrasi()} ><Text style={{ color: '#fff' }}> Registrasi </Text></Button>
                    }

                </Form>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFF',
        paddingTop : 20
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});