import React from 'react';
import { AsyncStorage, View } from "react-native";
import { Item, Label, Input, Button, Text, Spinner, Textarea } from "native-base";
import { Actions } from "react-native-router-flux";
import axios from "axios";

export default class Saran extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nik: '',
            saran: '',
        }
    }

    componentDidMount() {
        this.getNama();
    }

    static navigationOptions = {
        title: 'Saran dan Masukan',
    };

    getNama = async () => {
        let nik = await AsyncStorage.getItem('nik');
        this.setState({
            nik: nik,
            isLoading: false
        });
    }

    kirimSaran = () => {
        if (this.state.saran == '') {
            alert("Anda belum Menuliskan Saran");
        } else {

            this.setState({ isLoading: true })

            const data = new FormData();
            const { nik, saran } = this.state;
            data.append('nik', nik);
            data.append('nama', saran);
            axios.get('https://dinartech.com/simpus/api/create_saran/'+nik+'/'+saran)
                .then(res => {
                    this.setState({ isLoading: false })
                    Alert.alert(
                        'kirim saran berhasil',
                        'Terima Kasih, Saran anda tutun membangun Puskemas Tebas',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    Actions.pop();
                                }
                            },
                        ],
                        { cancelable: false }
                    )
                })

        }
    }

    render() {
        const { isLoading } = this.state
        return (
            <View style={{ padding: 20, backgroundColor: 'white', }}>
               
                    <Textarea
                        placeholder = "Saran, Masukan, dan Kritik"
                        rowSpan={5}
                        onChangeText={saran => this.setState({ saran })}
                    />
                
                {isLoading ?
                    (<View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}><Spinner color="blue" /></View>)
                    :
                    (<Button block style={{ marginBottom: 5, marginTop: 10, }} onPress={() => this.kirimSaran()}>
                        <Text>Kirim Saran</Text>
                    </Button>)
                }

            </View>
        );
    }

}