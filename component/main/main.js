import React from 'react';
import { StyleSheet, View, Image, AsyncStorage, TouchableOpacity, ScrollView, ToastAndroid, RefreshControl, Linking } from 'react-native';
import { Button, Content, Text, Card, CardItem, Body, spinner, Spinner, TabHeading } from 'native-base';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';

const refreshing = false;
export default class main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            antrians: [],
            nomor_antrian: '',
            nomor_ambulance: '',
            id: '',
            sisa: '',
            dipanggil: '',
            isOnline: false,
            isLoadingTotal: true,
            polis: [],
            isLoadingPolis: true,
            isLoadingAntrian: true,
            isTimeOut: true,
            jamSekarang: '',
            batasAwal: '',
            batasAkhir: '',
            isLoadingTimeOut: true,
        }
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        this.getAntrian();
        this.getMyAntrian();
        //axios.get('http://10.0.2.2.1:8000/api/getantrianpoli')
        // axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getantrianpoli')
        //     .then(res => {
        //         this.setState({ isLoadingPolis: false })
        //         if (res.data === null) {
        //             this.setState({
        //                 polis: [],
        //             })
        //         } else {
        //             this.setState({
        //                 polis: res.data,
        //             })
        //         }
        //     });

        // axios.get('https://puskesmastebas.sambas.go.id/simpus/api/nomorambulance')
        //     .then(res => {
        //         this.setState({
        //             nomor_ambulance: res.data.no_ambulance,
        //         })
        //     });


        /////
        let jam = new Date().getHours();
        let menit = new Date().getMinutes();
        let jammenit = jam + '.' + menit;

        this.setState({
            jamSekarang: jammenit
        })

        axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getantrianpoli')
            .then(res => {
                this.setState({ isLoadingPolis: false })
                if (res.data === null) {
                    this.setState({
                        polis: [],
                    })
                } else {
                    this.setState({
                        polis: res.data,
                    })
                }
            });

        axios.get('https://puskesmastebas.sambas.go.id/simpus/api/nomorambulance')
            .then(res => {
                this.setState({
                    nomor_ambulance: res.data.no_ambulance,
                    batasAwal: res.data.jam_awal,
                    batasAkhir: res.data.jam_akhir,
                })
                if (jammenit < Number(this.state.batasAwal) || jammenit > Number(this.state.batasAkhir || res.data.batas_online === '20')) {
                    this.setState({
                        isTimeOut: true,
                        isLoadingTimeOut: false,
                    })
                } else {
                    this.setState({
                        isTimeOut: false,
                        isLoadingTimeOut: false,
                    })
                }
            });

    }

    getAntrian() {
        axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getantrian')
            .then(res => {
                this.setState({ isLoadingTotal: false })
                if (res.data === null) {
                    this.setState({
                        antrians: [],
                    })
                } else {
                    this.setState({
                        antrians: res.data,
                        sisa: res.data[0].sisa,
                        dipanggil: res.data[0].dipanggil,
                    })
                }
            });
    }

    ambilAntrian = (val) => {
        axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getnomor/1/' + val)
            .then(res => {
                ToastAndroid.show("Berhasil mengambil nomor antrian", ToastAndroid.LONG);
                this.setState({
                    antrian: res.data.antrian,
                    id: res.data.id,
                    isOnline: true,
                })
                AsyncStorage.setItem('id_antrian', res.data.id);
            })
        this.getAntrian();
    }

    getMyAntrian = async () => {

        let id = await AsyncStorage.getItem('id_antrian');
        if (id !== null) {
            axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getmy/' + id)
                .then(res => {
                    this.setState({
                        antrian: res.data.antrian,
                        id: id,
                        isLoadingAntrian: false,
                    })
                    if (res.data.antrian !== '0') {
                        this.setState({
                            isOnline: true,
                            isLoadingAntrian: false,
                        })
                    }
                    //alert(res.data)
                })
        } else {

            this.setState({
                isLoadingAntrian: false,
            })

        }


    }

    _onRefresh = () => {
        this.setState({
            isLoadingPolis: true,
            isLoadingAntrian: true,
            isOnline: false,
            isLoadingTotal: true,
        })
        this.getAntrian();
        this.getMyAntrian();
        axios.get('https://puskesmastebas.sambas.go.id/simpus/api/getantrianpoli')
            .then(res => {
                this.setState({ isLoadingPolis: false })
                if (res.data === null) {
                    this.setState({
                        polis: [],
                    })
                } else {
                    this.setState({
                        polis: res.data,
                    })
                }
            });
    }

    refresh = () => {
        this.getAntrian();
        //this.getMyAntrian();
    }

    render() {
        const { antrians, antrian, sisa, id, isOnline, isLoadingTotal, polis, isLoadingPolis, isLoadingAntrian, nomor_ambulance, jamSekarang, batasAwal, batasAkhir, isTimeOut, dipanggil, isLoadingTimeOut } = this.state
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={this._onRefresh.bind(this)}
                        title="Loading..."
                    />
                }
            >
                <View style={{ backgroundColor: 'white', padding: 20, alignItems: 'center' }}>
                    <Image
                        source={require('./../assets/tbs.png')}
                        style={{ height: 50, width: 200, marginBottom: 30 }}
                    />
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', height: 60, width: '100%', marginBottom: 60 }}>
                        <View style={{ width: '25%', height: 90, padding: 5, alignItems: 'center' }}>
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderRadius: 7, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}
                                onPress={() => Actions.berita()}
                            >
                                <Image
                                    source={require('./../assets/hospital.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 12 }}>Berita</Text>
                        </View>
                        <View style={{ width: '25%', height: 90, padding: 5, alignItems: 'center' }}>
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderRadius: 5, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}
                                onPress={() => { Linking.openURL('tel:' + nomor_ambulance); }}
                            >
                                <Image
                                    source={require('./../assets/ambulance.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 12 }}>Ambulance</Text>
                        </View>
                        <View style={{ width: '25%', height: 90, padding: 5, alignItems: 'center' }}>
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderRadius: 5, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}
                                onPress={() => Actions.saran()}
                            >
                                <Image
                                    source={require('./../assets/computer.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 12 }}>Saran</Text>
                        </View>
                        <View style={{ width: '25%', height: 90, padding: 5, alignItems: 'center' }}>
                            <TouchableOpacity
                                style={{ borderWidth: 1, borderRadius: 5, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}
                                onPress={() => Actions.web()}
                            >
                                <Image
                                    source={require('./../assets/medicine.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 12 }}>Website</Text>
                        </View>
                    </View>
                    <View style={{ width: '100%' }}>
                        
                        {isTimeOut ? (
                            <View style={{ width: '100%', backgroundColor: '#ff7675', marginBottom: 10, borderRadius: 7, padding: 15 }}>{isLoadingTimeOut ? (<View><Spinner color='blue' /></View>):(<View><Text style={{ fontSize: 12, fontWeight: 'bold', alignSelf: 'center', }}>Jadwal Antrian Tidak Tersedia Atau Antrian Online Sudah Memenuhi Kuota</Text></View>) }</View>
                        ) : (<View>
                            {isLoadingTotal ?
                                (<View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}><Spinner color="blue" /></View>)
                                :
                                (antrians.map((antrian, index) =>
                                    <View key={index} style={{ width: '100%', backgroundColor: '#81ecec', marginBottom: 10, borderRadius: 7, padding: 10 }}>
                                        <View style={{ width: '100%', borderBottomColor: '#00cec9', borderBottomWidth: 1, padding: 5 }}>
                                            <Text style={{ fontSize: 12, fontWeight: 'bold', alignSelf: 'center', }}>Antrian Loket Pendaftaran Hari ini</Text>
                                        </View>
                                        <View style={{ padding: 10, flexDirection: 'row', alignItems: 'center' }}>

                                            <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 5, paddingBottom: 5, width: '33%' }}>
                                                <Text style={{ fontSize: 35 }}> {antrian.dipanggil} </Text>
                                                <Text style={{ fontSize: 12 }}> Dipanggil </Text>
                                            </View>

                                            <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 10, paddingBottom: 10, width: '34%' }}>
                                                <Text style={{ fontSize: 35 }}> {antrian.total_antrian} </Text>
                                                <Text style={{ fontSize: 12 }}> Total Antrian </Text>
                                            </View>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 5, paddingBottom: 5, width: '33%' }}>
                                                <   Text style={{ fontSize: 35 }}> {antrian.sisa} </Text>
                                                <Text style={{ fontSize: 12 }}> Sisa Antrian </Text>
                                            </View>
                                        </View>
                                    </View>
                                ))}
                            {isLoadingAntrian ?
                                (<View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}><Spinner color="blue" /></View>)
                                :
                                isOnline ?
                                    (<View style={{ width: '100%', padding: 10, backgroundColor: '#ffcccc', borderRadius: 7 }}>
                                        <View style={{ width: '100%', borderBottomColor: '#ffb8b8', borderBottomWidth: 1, padding: 5 }}>
                                            <Text style={{ fontSize: 12, fontWeight: 'bold', alignSelf: 'center' }}>Antrian Anda</Text>
                                        </View>
                                        <View style={{ width: '100%', flexDirection: 'row' }}>
                                            <View style={{ width: '70%', padding: 20, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontSize: 40, fontWeight: 'bold', }}>{antrian}</Text>
                                                <Text style={{ fontSize: 10, fontWeight: 'bold', }}>{id}</Text>
                                            </View>
                                            <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ fontSize: 40, fontWeight: 'bold', }}>{antrian.substring(1)-dipanggil}</Text>
                                                <Text style={{ fontSize: 10, fontWeight: 'bold', }}>Sisa Antrian</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: '100%', marginTop: 10, }}>
                                            <Button block danger bordered onPress={() => this.refresh()} >
                                                <Text>Refresh</Text>
                                            </Button>
                                        </View>
                                        <View style={{ width: '100%', padding: 5 }}>
                                            <Text style={{ fontSize: 12, fontWeight: 'bold', }}>Pastikan Anda Datang Sebelum Antrian Anda Tersisa 3 , dan Pastikan Klik Tombol Refresh Agar Data Antrian Anda Terbaharui</Text>
                                        </View>
                                    </View>)
                                    : (<View style={{ width: '100%', marginTop: 10, }}>
                                        <Button block style={{ marginBottom: 5 }} onPress={() => this.ambilAntrian(0)}>
                                            <Text>Antrian Umum</Text>
                                        </Button>
                                        <Button block success onPress={() => this.ambilAntrian(1)} >
                                            <Text>Antrian BPJS</Text>
                                        </Button>
                                    </View>)}
                        </View>
                            )}


                    </View>

                </View>
                <View>
                    {isLoadingPolis ?
                        (<View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}><Spinner color='blue' /></View>)
                        : (<View style={{ width: '100%', padding: 10 }}>
                            {polis.map((poli, index) =>
                                <View key={index} style={{ width: '100%', backgroundColor: 'white', marginTop: 10, borderRadius: 7, shadowColor: '#dfe6e9', }}>

                                    <View style={{ width: '100%', padding: 10, borderBottomWidth: 1, borderBottomColor: '#b2bec3' }}>
                                        <Text style={{ fontSize: 12, fontWeight: 'bold', }}> {poli.nama_pelayanan} - {poli.nama} </Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 5, paddingBottom: 10, width: '33%' }}>
                                            <Text style={{ fontSize: 35 }}> {poli.total} </Text>
                                            <Text style={{ fontSize: 12 }}> Dipanggil </Text>
                                        </View>

                                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 10, paddingBottom: 10, width: '34%' }}>
                                            <Text style={{ fontSize: 35 }}> {poli.dipanggil} </Text>
                                            <Text style={{ fontSize: 12 }}> Total Antrian </Text>
                                        </View>
                                        <View style={{ alignItems: 'center', justifyContent: 'center', paddingRight: 10, paddingLeft: 10, paddingTop: 5, paddingBottom: 10, width: '33%' }}>
                                            <   Text style={{ fontSize: 35 }}> {poli.sisa} </Text>
                                            <Text style={{ fontSize: 12 }}> Sisa Antrian </Text>
                                        </View>
                                    </View>
                                    <View>

                                    </View>

                                </View>
                            )}
                        </View>)

                    }
                </View>
            </ScrollView>
        );
    }


}