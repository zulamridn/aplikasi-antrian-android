import React from "react";
import { WebView } from "react-native-webview";


export default class Web extends React.Component{
    static navigationOptions = {
        header: null,
    };
    render(){
        return (
            <WebView
              source={{uri: 'https://puskesmastebas.sambas.go.id'}}
            />
          );
    }
}