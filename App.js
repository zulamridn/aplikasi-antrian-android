import React from 'react';
import {StyleSheet, View, Image, AsyncStorage, Text } from 'react-native';
import Registrasi from './component/registrasi';
import Route from './component/route';
import { Router, Scene } from 'react-native-router-flux';

export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      isSplash : true,
      nik : ''
    }
  }

  componentDidMount(){
    setTimeout(()=>{this.setState({isSplash: false})}, 3000);
    AsyncStorage.getItem('nik').then(val => { this.setState({ nik : val }) } )
  }

  render() {
    const {isSplash, nik} = this.state;
    if(isSplash){
      return <View style={styles.container}>
              <Image  
            source={require('./assets/logo.png')}
            style ={{ width:120, height:140 , marginBottom:20}} />
            <Text style={{ fontWeight:'bold' }}>PUSKESMAS TEBAS</Text>
            <Text style={{ fontWeight:'bold' }}>KABUPATEN SAMBAS</Text>
      </View>
    }else{
      if(nik !== null){
        return (<Route />); 
      }else{
        return (<Registrasi />);
      }
    }
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

